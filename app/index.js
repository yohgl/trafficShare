import React from 'react';
import { DrawerNavigator, StackNavigator } from 'react-navigation';
import { Icon } from 'react-native-elements';
import SignInScreen from './SignInScreen';
import SignUpScreen from './SignUpScreen';
import ConfirmationScreen from './ConfirmationScreen';
import WelcomeScreen from './WelcomeScreen';
import GeolocationScreen from './GeolocationScreen';
import HomeScreen from './HomeScreen';
import LiveCaptureScreen from './LiveCaptureScreen';

const AuthenStack = StackNavigator({
  SignIn: {
    screen: SignInScreen,
    navigationOptions: { title: 'Sign In', header: null},
  },
  SignUp: {
    screen: SignUpScreen,
    navigationOptions: { title: 'Sign Up', header: null},
  },
  ConfirmCode: {
    screen: ConfirmationScreen,
    navigationOptions: { title: 'Confirmation Code', header: null},
  },
  WelcomeScreen: {
    screen: WelcomeScreen,
    navigationOptions: { title: 'Welcome to Traffic Share', header: null},
  },
});

const Main = DrawerNavigator({
  HomeScreen: {
    screen: HomeScreen,
    navigationOptions: {
      drawerLabel: 'Home',
      drawerIcon:  ({ tintColor }) => <Icon name="home" size={35} color={tintColor} />,
    },
  },
  AuthenScreen: {
    screen: AuthenStack,
    navigationOptions: {
      drawerLabel: 'Authentication',
      drawerIcon:  ({ tintColor }) => <Icon name="fingerprint" size={35} color={tintColor} />,
    },
  },
  Geolocation: {
    screen: GeolocationScreen,
    navigationOptions: {
      drawerLabel: 'Maps',
      drawerIcon:  ({ tintColor }) => <Icon name="map" size={35} color={tintColor} />,
    },
  },
  LiveCapture: {
    screen: LiveCaptureScreen,
    navigationOptions: {
      drawerLabel: 'Maps',
      drawerIcon:  ({ tintColor }) => <Icon name="camera" size={35} color={tintColor} />,
    },
  },
});
export default Main;