
import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Modal,
  TouchableHighlight,
  Image
} from 'react-native';
import { List, ListItem } from 'react-native-elements'
import AWS from 'aws-sdk/dist/aws-sdk-react-native';

const list = [
  {
    name: 'อโศก',
    avatar_url: 'https://s3.amazonaws.com/tf-share-now/20170628/tf1.jpg',
    subtitle: 'ติดเหมือนเดิม'
  },
  {
    name: 'พร้อมพงษ์',
    avatar_url: 'https://s3.amazonaws.com/tf-share-now/20170628/tf2.jpg',
    subtitle: 'หนักเลยยยยย'
  },
  {
    name: 'ทางด่วนกม9',
    avatar_url: 'https://s3.amazonaws.com/tf-share-now/20170628/tf3.jpg',
    subtitle: 'พอไหลๆ'
  },
  {
    name: 'สุขสวัสดิ์ 26',
    avatar_url: 'https://s3.amazonaws.com/tf-share-now/20170628/tf4.jpg',
    subtitle: 'สบ๊ายยยยย'
  },
];


export default class LiveCaptureScreen extends React.Component {
  constructor() {
    super();

    var dynamodb = new AWS.DynamoDB({ 'region': 'us-east-1', 'accessKeyId' : 'accessKeyId', 'secretAccessKey': 'secretAccessKey' });
    console.log('dynamodb:', dynamodb);
    var params = {
      Key: {},
      TableName: 'tflist', /* required */
      AttributesToGet: [
        'id',
        'title',
        'subtitle',
        'url'
        /* more items */
      ]
    };
    dynamodb.getItem(params, function (err, data) {
      if (err) console.log(err, err.stack); // an error occurred
      else console.log(data);           // successful response
    });
  }

  state = {
    modalVisible: false,
    uri: 'https://facebook.github.io/react/img/logo_og.png'
  }

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }
  displayPopup(url) {
    this.setState({ uri: url });
    this.setModalVisible(true);
  }

  render() {
    return (
      <View style={{ marginTop: 15 }}>
        <Modal
          animationType={"slide"}
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => console.log('close')}
        >
          <View style={styles.displayPopup}>
            <View>
              <Image ref="displayModal" source={{ uri: this.state.uri }}
                style={{ width: 300, height: 300 }} />
              <TouchableHighlight style={styles.container} onPress={() => { this.setModalVisible(!this.state.modalVisible) }}>
                <Text>close</Text>
              </TouchableHighlight>
            </View>
          </View>
        </Modal>

        <List containerStyle={{ marginBottom: 20 }}>
          {
            list.map((l, i) => (
              <ListItem
                roundAvatar
                avatar={{ uri: l.avatar_url }}
                key={i}
                title={l.name}
                subtitle={l.subtitle}
                onPress={() => this.displayPopup(l.avatar_url)}
              />
            ))
          }
        </List>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
  },
  link: {
    fontSize: 5,
  },
  displayPopup: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',

  }
});

//export default LiveCaptureScreen;