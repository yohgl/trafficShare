import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import MapView from 'react-native-maps';
import {Location} from 'expo';
import CustomCallout from './maps/CustomCallout';

const GEOLOCATION_OPTIONS = { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 };
const { width, height } = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

export default class MapScreen extends React.Component {

  locationChanged = (location) => {
    region = {
      latitude: location.coords.latitude,
      longitude: location.coords.longitude,
      latitudeDelta: LATITUDE_DELTA,
      longitudeDelta: LONGITUDE_DELTA,
    },
    this.setState({location, region})
    console.log('locationChanged this.state:',this.state.region);
    this.refs.map.animateToRegion(this.state.region);
  }
  constructor(props) {
    super(props);
    Location.watchPositionAsync(GEOLOCATION_OPTIONS, this.locationChanged);
    this.state = {}   
  }


  render() {
    return (
      <View style={styles.container}>
        <MapView
          ref="map"
          provider={this.props.provider}
          style={styles.map}
          initialRegion={this.state.region}
          followsUserLocation={true}
          showsUserLocation={true}
          showsMyLocationButton={true}
        >
        </MapView>
      </View>
    );
  }
}

MapScreen.propTypes = {
  provider: MapView.ProviderPropType,
};

const styles = StyleSheet.create({
  customView: {
    width: 140,
    height: 100,
  },
  plainView: {
    width: 60,
  },
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  }
});