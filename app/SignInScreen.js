
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import AWS from 'aws-sdk/dist/aws-sdk-react-native';

import { 
    AuthenticationDetails, 
    CognitoUser, 
    CognitoUserAttribute, 
    CognitoUserPool 
} from '../lib/aws-cognito-identity';

var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider(
  {
    region: "us-east-1",
    identity_pool_id: "us-east-1_QpLR8vnbW"
  });

const background = require("../screens/signup/signup_bg.png");
const backIcon = require("../screens/signup/back.png");
const personIcon = require("../screens/signup/signup_person.png");
const lockIcon = require("../screens/signup/signup_lock.png");
const emailIcon = require("../screens/signup/signup_email.png");
const birthdayIcon = require("../screens/signup/signup_birthday.png");

export default class SignInScreen extends Component {

  constructor() {
    super();

    this.state = {
      isLoading: false,
      username: '',
      email: '',
      password: ''
    };

    this._onPress = this._onPress.bind(this);
  }
  _onPress() {
    var authenticationData = {
      Username: this.state.username,
      Password: this.state.password,
    };
    console.log('authenticationData:', authenticationData);
    var authenticationDetails = new AuthenticationDetails(authenticationData);
    var poolData = {
      UserPoolId: "us-east-1_QpLR8vnbW", // Your user pool id here
      ClientId: '6qkk62ap5esfji3pu6v5jrhrvq' // Your client id here
    };
    console.log('poolData:', poolData);
    var userPool = new CognitoUserPool(poolData);
    var userData = {
      Username: this.state.username,
      Pool: userPool
    };
    var cognitoUser = new CognitoUser(userData);
    console.log('cognitoUser:', cognitoUser);
    var _self = this;
    cognitoUser.authenticateUser(authenticationDetails, {
      onSuccess: function (result) {
        console.log('access token + ' + result.getAccessToken().getJwtToken());
        
        AWS.config.credentials = new AWS.CognitoIdentityCredentials({
          IdentityPoolId: 'us-east-1_QpLR8vnbW', // your identity pool id here 
          Logins: {
            'cognito-idp.us-east-1.amazonaws.com/us-east-1_QpLR8vnbW': result.getIdToken().getJwtToken()
          }
        });
        _self.props.navigation.navigate('WelcomeScreen');
      },

      onFailure: function (err) {
        alert(err);
      },

    });

  }

  render() {
    const { navigate } = this.props.navigation;
    
    return (

      <View style={styles.container}>
        <Image source={background} style={[styles.container, styles.bg]} resizeMode="cover">
          <View style={styles.headerContainer}>

            <View style={styles.headerTitleView}>
              <Text style={styles.titleViewText}>Sign In</Text>
            </View>

          </View>

          <View style={styles.inputsContainer}>

            <View style={styles.inputContainer}>
              <View style={styles.iconContainer}>
                <Image source={personIcon} style={styles.inputIcon} resizeMode="contain" />
              </View>
              <TextInput
                style={[styles.input, styles.whiteFont]}
                placeholder="Username"
                placeholderTextColor="#FFF"
                underlineColorAndroid='transparent'
                onChangeText={(username) => this.setState({ username })}
              />
            </View>

            <View style={styles.inputContainer}>
              <View style={styles.iconContainer}>
                <Image source={lockIcon} style={styles.inputIcon} resizeMode="contain" />
              </View>
              <TextInput
                secureTextEntry={true}
                style={[styles.input, styles.whiteFont]}
                placeholder="Password"
                placeholderTextColor="#FFF"
                onChangeText={(password) => this.setState({ password })}
              />
            </View>

          </View>

          <View style={styles.footerContainer}>

            <TouchableOpacity onPress={this._onPress}>
              <View style={styles.login}>
                <Text style={styles.whiteFont}>Login</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity>
              <View style={styles.signup}>
                <Text style={styles.greyFont}>Don't Have an Account?<Text style={styles.whiteFont} onPress={() => navigate('SignUp', { param1: 'xxx' })}> Create Account</Text></Text>
              </View>
            </TouchableOpacity>
          </View>
        </Image>
      </View>
    );
  }
}


let styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  bg: {
    paddingTop: 30,
    width: null,
    height: null
  },
  headerContainer: {
    flex: 1,
  },
  inputsContainer: {
    flex: 3,
    marginTop: 50,
  },
  footerContainer: {
    flex: 1
  },
  headerIconView: {
    marginLeft: 10,
    backgroundColor: 'transparent'
  },
  headerTitleView: {
    backgroundColor: 'transparent',
    marginTop: 25,
    marginLeft: 25,
  },
  titleViewText: {
    fontSize: 30,
    color: '#ecf0f1',
  },
  inputs: {
    paddingVertical: 20,
  },
  inputContainer: {
    borderWidth: 1,
    borderBottomColor: '#CCC',
    borderColor: 'transparent',
    flexDirection: 'row',
    height: 75,
  },
  iconContainer: {
    paddingHorizontal: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputIcon: {
    width: 30,
    height: 30,
  },
  input: {
    flex: 1,
    fontSize: 20,
  },
  login: {
    backgroundColor: '#FF3366',
    paddingVertical: 25,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 15,
  },
  signup: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
  greyFont: {
    color: '#D8D8D8'
  },
  whiteFont: {
    color: '#FFF'
  }
})
