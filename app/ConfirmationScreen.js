
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import AWS from 'aws-sdk/dist/aws-sdk-react-native';

var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider(
  {
    region: "us-east-1",
    identity_pool_id: "us-east-1_QpLR8vnbW"
  });

const background = require("../screens/signup/signup_bg.png");
const backIcon = require("../screens/signup/back.png");
const personIcon = require("../screens/signup/signup_person.png");
const lockIcon = require("../screens/signup/signup_lock.png");
const emailIcon = require("../screens/signup/signup_email.png");
const birthdayIcon = require("../screens/signup/signup_birthday.png");

export default class ConfirmationScreen extends Component {

  constructor(props) {
    super(props);
    console.log('@confirm : ',props.username);
    this.state = {
      isLoading: false,
      username: props.username,
      confirmCode: ''
    };

    this._onPress = this._onPress.bind(this);
  }
  _onPress() {
    var params = {
      ClientId: '6qkk62ap5esfji3pu6v5jrhrvq', /* Fixed from Cognito */
      ConfirmationCode: this.state.confirmCode,
      Username: this.state.username
    };
    console.log('confirmSignUp : ', params);
    var _self = this;
    cognitoidentityserviceprovider.confirmSignUp(params, function (err, data) {
      if (err) {
        console.log(err, err.stack); // an error occurred
      }
      else {
        console.log(data);           // successful response
        _self.props.navigation.navigate('WelcomeScreen');
      }
    });

  }

  render() {
    return (

      <View style={styles.container}>
        <Image source={background} style={[styles.container, styles.bg]} resizeMode="cover">
          <View style={styles.headerContainer}>

            <View style={styles.headerTitleView}>
              <Text style={styles.titleViewText}>Confirmation Sign Up</Text>
            </View>

          </View>

          <View style={styles.inputsContainer}>

            <View style={styles.inputContainer}>
              <View style={styles.iconContainer}>
                <Image source={personIcon} style={styles.inputIcon} resizeMode="contain" />
              </View>
              <TextInput
                style={[styles.input, styles.whiteFont]}
                placeholder="Username"
                placeholderTextColor="#FFF"
                underlineColorAndroid='transparent'
                onChangeText={(username) => this.setState({ username })}
                value={this.state.username}
              />
            </View>

            <View style={styles.inputContainer}>
              <View style={styles.iconContainer}>
                <Image source={lockIcon} style={styles.inputIcon} resizeMode="contain" />
              </View>
              <TextInput
                secureTextEntry={true}
                style={[styles.input, styles.whiteFont]}
                placeholder="Confirmation Code"
                placeholderTextColor="#FFF"
                onChangeText={(confirmCode) => this.setState({ confirmCode })}
              />
            </View>

          </View>

          <View style={styles.footerContainer}>

            <TouchableOpacity onPress={this._onPress}>
              <View style={styles.confirm}>
                <Text style={styles.whiteFont}>Confirm</Text>
              </View>
            </TouchableOpacity>

          </View>
        </Image>
      </View>
    );
  }
}


let styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  bg: {
    paddingTop: 30,
    width: null,
    height: null
  },
  headerContainer: {
    flex: 1,
  },
  inputsContainer: {
    flex: 3,
    marginTop: 50,
  },
  footerContainer: {
    flex: 1
  },
  headerIconView: {
    marginLeft: 10,
    backgroundColor: 'transparent'
  },
  headerTitleView: {
    backgroundColor: 'transparent',
    marginTop: 25,
    marginLeft: 25,
  },
  titleViewText: {
    fontSize: 30,
    color: '#ecf0f1',
  },
  inputs: {
    paddingVertical: 20,
  },
  inputContainer: {
    borderWidth: 1,
    borderBottomColor: '#CCC',
    borderColor: 'transparent',
    flexDirection: 'row',
    height: 75,
  },
  iconContainer: {
    paddingHorizontal: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputIcon: {
    width: 30,
    height: 30,
  },
  input: {
    flex: 1,
    fontSize: 20,
  },
  confirm: {
    backgroundColor: '#FF3366',
    paddingVertical: 25,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 15,
  },
  whiteFont: {
    color: '#FFF'
  }
})
